fun main() {
    var lambda = {param1: Int, param2: Int, param3: Boolean ->
        if (param3) println(param1) else println(param2)
    }
    var a = lambda(1, 2, false)


    var lambda2 = {println("Hello")}
    lambda2()

    var b: Int = 5
    var lambda3 = {
        b++
    }
    println(b)
    lambda3()
    println(b)
}